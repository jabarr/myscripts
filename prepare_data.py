#!/usr/bin/env python
import argparse
from numpy.lib.recfunctions import structured_to_unstructured as s2u
import pandas as pd
import os
import glob
from pathlib import Path

import uproot
import awkward as ak
from coffea.nanoevents.methods import vector
ak.behavior.update(vector.behavior)

import pyarrow as pa
import pyarrow.parquet as pq
import numpy as np

vecDict = {'pt':'pt', 'eta':'eta','phi':'phi', 'mass':'m'}


def parse_args(args):
    parser = argparse.ArgumentParser(
        description="A script to convert root files to parquet files",
    )
    parser.add_argument(
        "--samples",
        "-s",
        type=Path,
        help="Directory samples to be converted are stored. Note: will search subdirectories too!",
        required=True,
    )
    parser.add_argument(
        "--outDir",
        type=Path,
        help="Directory to store parquet files",
        default="../parquetOut",
    )
    parser.add_argument(
        "--year",
        "-y",
        type=int,
        help="Year of data taking",
        required=True,
    )
    parser.add_argument(
        "--mc",
        help="Is this MC?",
        action="store_true"
    )
    parser.add_argument(
        "--outFile",
        "-o",
        type=Path,
        help="Output file name",
        required=True,
    )
    parser.add_argument(
        "--overwrite",
        "-f",
        help="Overwrite existing parquet file",
        action="store_true"
    )
    parser.add_argument(
        "--post",
        "-p",
        help="Apply extra processing for use in flow training",
        action="store_true"
    )

    return parser.parse_args(args)


def get_4vectors(array):
    return ak.zip( { k: array[v] for k,v in vecDict.items() }, with_name="PtEtaPhiMLorentzVector" )

def baseline_selections(fname, 
                        btagger=["GN2v00LegacyWP", 77],
                        xbbtagger=['GN2Xv01',70],
                        year=2018,
                        mc=True,
                       ):
  
    ntup = uproot.open(fname)
    ntup = ntup['AnalysisMiniTree']

    # sometimes ntup is empty, return empty dataframe
    if len(ntup) == 0:
        print(f"File {fname} is empty")
        return pd.DataFrame(), pd.DataFrame()

    if year == 2016:
        lrj_trig = "trigPassed_HLT_j420_a10_lcw_L1J100"
        trig_2b2j = "trigPassed_HLT_2j35_bmv2c2060_split_2j35_L14J15p0ETA25"
        trig_2b1j = "trigPassed_HLT_j100_2j55_bmv2c2060_split"
        trig_1b = "trigPassed_HLT_j225_bmv2c2060_split"
        trig_2bHT = "trigPassed_HLT_2j35_bmv2c2060_split_2j35_L14J15p0ETA25"

    if year == 2017:
        lrj_trig = "trigPassed_HLT_j420_a10t_lcw_jes_40smcINF_L1J100"
        trig_2b2j = "trigPassed_HLT_2j15_gsc35_bmv2c1040_split_2j15_gsc35_boffperf_split_L14J15p0ETA25"
        trig_2b1j = "trigPassed_HLT_j110_gsc150_boffperf_split_2j35_gsc55_bmv2c1070_split_L1J85_3J30"
        trig_1b = "trigPassed_HLT_j225_gsc300_bmv2c1070_split"
        trig_2bHT = "trigPassed_HLT_2j35_gsc55_bmv2c1050_split_ht300_L1HT190_J15s5pETA21"

    if year == 2018:
        lrj_trig = 'trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1J100'
        trig_2b2j = 'trigPassed_HLT_2j35_bmv2c1060_split_2j35_L14J15p0ETA25'
        trig_2b1j = 'trigPassed_HLT_j110_gsc150_boffperf_split_2j45_gsc55_bmv2c1070_split_L1J85_3J30'
        trig_1b = 'trigPassed_HLT_j225_gsc275_bhmv2c1060_split'
        trig_2bHT = 'trigPassed_HLT_2j45_gsc55_bmv2c1050_split_ht300_L1HT190_J15s5pETA21'
        
    triggers = [
        lrj_trig,
        trig_2b2j,
        trig_2b1j,
        trig_1b,
        trig_2bHT,
    ]

    # Each trigger is a True or False boolean, use .any(axis=1) to perform OR of all triggers
    trig_decs = ntup.arrays(triggers).to_numpy()
    trig_or = s2u(trig_decs).any(axis=1)

    ecols = ['runNumber', 'eventNumber']
    ecols += triggers

    evts = pd.DataFrame(ntup.arrays(ecols).to_numpy())

    # Create a cutflow to record the number of events passing each selection
    cutflow = pd.DataFrame()
    cutflow['nEvents'] = [len(evts)]
    
    # First load the large-R jets
    lrj_cols = ['pt', 'eta', 'phi', 'm',
                'GN2Xv01_phbb', 'GN2Xv01_phcc', 'GN2Xv01_ptop', 'GN2Xv01_pqcd']
    
    lrj_cols_full = [f'recojet_antikt10UFO_NOSYS_{v}' for v in lrj_cols]
    
    if mc:
        lrj_cols += ['lrj_flav']
        lrj_cols_full += [f'recojet_antikt10UFO_NOSYS_R10TruthLabel_R22v1']

    lrjalias = {k: f'{v} / 1000' if (k=='pt' or k=='m') else v  
                  for k,v in zip(lrj_cols,lrj_cols_full)}

    lrj_array = ntup.arrays(lrj_cols, aliases=lrjalias, cut=f'(pt > 250) & (abs(eta) < 2.0) & (m > 70)')
    
    
    # Now the small-R jets
    j_cols = ['pt', 'eta', 'phi', 'm', 'NNJvtPass', 'tag_77', 'tag_85', 'tag_77_new', 'tag_85_new']
    j_cols_full = [f'recojet_antikt4PFlow_NOSYS_{v}' for v in 
                      ['pt','eta','phi','m', 'NNJvtPass',
                       f'ftag_select_GN2v00LegacyWP_FixedCutBEff_77',
                       f'ftag_select_GN2v00LegacyWP_FixedCutBEff_85',
                       f'ftag_select_GN2v00NewAliasWP_FixedCutBEff_77',
                       f'ftag_select_GN2v00NewAliasWP_FixedCutBEff_85',
                       ]]
    
    if mc:
        j_cols += ['flav']
        j_cols_full += [f'recojet_antikt4PFlow_NOSYS_HadronConeExclTruthLabelID']


    jalias = {k: f'{v} / 1000' if (k=='pt' or k=='m') else v  
                  for k,v in zip(j_cols,j_cols_full)}

    j_array = ntup.arrays(j_cols, aliases=jalias, cut=f'(pt > 40) & (abs(eta) < 2.5) & NNJvtPass')

    if mc:
        truth_cols = [
            "truth_H1_pdgId", "truth_H2_pdgId",
            "truth_H1_pt", "truth_H2_pt",
            "truth_H1_eta", "truth_H2_eta",
            "truth_H1_phi", "truth_H2_phi",
            "truth_H1_m", "truth_H2_m",
            #"mcEventWeights"
            
        ]
        truth_array = ntup.arrays(truth_cols)
        
        # Convert necessary Awkward Array columns to Pandas Series
        H1_pdgId = pd.Series(truth_array['truth_H1_pdgId'])
        H2_pdgId = pd.Series(truth_array['truth_H2_pdgId'])

        # Identify Higgs and S particles
        is_H1_Higgs = H1_pdgId == 25

        # Assign properties to Higgs and S
        for prop in ['pt', 'eta', 'phi', 'm']:
            out_prop = prop if prop != 'm' else 'mass'
        
            evts[f'truth_H_{out_prop}'] = pd.Series(truth_array[f'truth_H1_{prop}']).where(is_H1_Higgs, pd.Series(truth_array[f'truth_H2_{prop}']))
            evts[f'truth_S_{out_prop}'] = pd.Series(truth_array[f'truth_H2_{prop}']).where(is_H1_Higgs, pd.Series(truth_array[f'truth_H1_{prop}']))
            
            if out_prop in ['pt', 'mass']:
                evts[f'truth_H_{out_prop}'] = evts[f'truth_H_{out_prop}'] / 1000
                evts[f'truth_S_{out_prop}'] = evts[f'truth_S_{out_prop}'] / 1000
            
 
            # TODO: figure out what is wrong with mcEventWeights - update: use first element of array
            # TODO:  do deltaR matching to candidates maybe? then use that info how?


    # apply the trigger decision mask
    j_array = j_array[trig_or]
    lrj_array = lrj_array[trig_or]
    evts = evts[trig_or].reset_index(drop=True)
    cutflow['trig'] = [len(evts)]

    # require at least 1 large-R jet
    evts["nlrjets"] = ak.num(lrj_array["pt"]).to_numpy()
    lrj_mask = evts["nlrjets"] >= 1

    j_array = j_array[lrj_mask]
    lrj_array = lrj_array[lrj_mask]
    evts = evts[lrj_mask].reset_index(drop=True)
    cutflow['nlrjets'] = [len(evts)]
    
    # Create 4-vectors from the arrays
    j4 = get_4vectors(j_array)
    lrj4 = get_4vectors(lrj_array)

    # Calculates cartesian product between j4 and lrj4 e.g. if structure is
    # j4 = [[1,2], [1,2,3]], lrj4 = [[a], [a,b]], pairs returns
    # [[(1,a), (2,a)], [(1,a), (1,b), (2,a) (2,b), (3,a) (3,b)]]
    # Only do overlap removal with respect to leading large-R jet as it is H-cand, note [:,0]!!! 
    pairs = ak.cartesian({"small-R" : j4, "large-R" : lrj4[:,0]}, nested=True)
    delta_r = pairs["small-R"].delta_r(pairs["large-R"])
    mask = ak.any(delta_r < 1, axis=2)

    # Apply the mask to j4 to filter out jets with delta R < 1
    j4 = j4[~mask]
    j_array = j_array[~mask]

    # require at least 2 small-R jets for S-candidate
    evts["njets"] = ak.num(j_array["pt"]).to_numpy()
    j_mask = evts["njets"] > 2
   
    j_array = j_array[j_mask]
    lrj_array = lrj_array[j_mask]
    evts = evts[j_mask].reset_index(drop=True)
    cutflow['ORnjets'] = [len(evts)]
    

    # sort by jet pT's
    idx_j = ak.argsort(j_array['pt'],ascending=False)
    j_array = j_array[idx_j]

    idx_lrj = ak.argsort(lrj_array['pt'],ascending=False)
    lrj_array = lrj_array[idx_lrj]
    
    # recalculate 4-vectors after masking and sorting has been applied
    j4 = get_4vectors(j_array)
    lrj4 = get_4vectors(lrj_array)
    
    H_cand = lrj4[:, 0]
    S_cand = j4[:, 0] + j4[:, 1]
    X_cand = S_cand + H_cand
    
    S_cand_mask = S_cand.mass > 50
    
    j_array = j_array[S_cand_mask]
    lrj_array = lrj_array[S_cand_mask]
    evts = evts[pd.Series(S_cand_mask)]
    
    H_cand = H_cand[S_cand_mask]
    S_cand = S_cand[S_cand_mask]
    X_cand = X_cand[S_cand_mask]
    
    evts["H_pt"] = H_cand.pt
    evts["H_eta"] = H_cand.eta
    evts["H_phi"] = H_cand.phi
    evts["H_mass"] = H_cand.mass
    evts["H_energy"] = H_cand.energy
    
    evts["S_pt"] = S_cand.pt
    evts["S_eta"] = S_cand.eta
    evts["S_phi"] = S_cand.phi
    evts["S_mass"] = S_cand.mass
    evts["S_energy"] = S_cand.energy
    
    evts["X_pt"] = X_cand.pt
    evts["X_eta"] = X_cand.eta
    evts["X_phi"] = X_cand.phi
    evts["X_mass"] = X_cand.mass
    evts["X_energy"] = X_cand.energy

    # calculate Xbb discriminant on H-candidate from GN2Xv01 tagger
    phbb = lrj_array['GN2Xv01_phbb'][:,0]
    phcc = lrj_array['GN2Xv01_phcc'][:,0]
    ptop = lrj_array['GN2Xv01_ptop'][:,0]
    pqcd = lrj_array['GN2Xv01_pqcd'][:,0]

    evts["XbbDisc"] = np.log(phbb / (0.02* phcc + 0.25*ptop + 0.73*pqcd))
    
    # only check the b-tagging of the leading 2 small-R jets as they are the S-candidate
    evts['btag_77'] = ak.sum(j_array['tag_77'][:, :2],axis=1).to_numpy()
    evts['btag_85'] = ak.sum(j_array['tag_85'][:, :2],axis=1).to_numpy()
    evts['btag_77_new'] = ak.sum(j_array['tag_77_new'][:, :2],axis=1).to_numpy()
    evts['btag_85_new'] = ak.sum(j_array['tag_85_new'][:, :2],axis=1).to_numpy()
 
    return evts, cutflow

def XbbPass(df, loose=False):
    
    if loose: # 2% qcd efficiency
        wp_vals = [
            1.7185105346074547, 
            1.7866253429531844,
            1.7784007043845094,
            1.7459373768055864,
            1.8014611031641354,
            1.8952259119892,
            2.1283586554030802,
            2.1137397513264307,
            2.2250428085451777,
            2.15066878063987,
            2.035149041954063
        ]
    
    else: # 1% qcd efficiency
        wp_vals = [
            2.8721525535528802,
            3.0921504671462228,
            3.084309968629308,
            3.079190165176007,
            3.106715503266313,
            3.1706762934206463,
            3.413375575868704,
            3.377601581313833,
            3.432274198085627,
            3.354155536052264,
            3.078055248444524
                  ]
        
        mass_bins = [0, 70, 90, 110, 130, 150, 170, 190, 210, 230, 250, 50000]
        df['mass_bin'] = pd.cut(df['H_mass'], bins=mass_bins, labels=range(len(mass_bins)-1))

        def check_wp(xbb_disc, wp_val):
            return xbb_disc > wp_val

        # Apply the check using transform
        # This creates a boolean mask that checks if each XbbDisc is greater than the corresponding wp_val
        mask = df.groupby('mass_bin')['XbbDisc'].transform(lambda x: check_wp(x, wp_vals[x.name]))

        # Assign the mask to the 'XbbPass' column
        df['XbbPass'] = mask
        
        return df
    

def apply_cuts(df, btag_wp="btag_77", unblind=False, blind=True, btags=2, mask_mass=False):
    
    btag_mask = df[btag_wp] == btags
    Xbb_mask = df["XbbPass"]
    
    # Define the mass mask for the blinded case (within the mass window)
    mass_mask = (df["H_mass"] < 110) | (df["H_mass"] >140)
    
    # Set two flags to be doublely sure you want to unblind!
    if (unblind and not blind):
        mass_mask = ~mass_mask

    if mask_mass:
        final_mask =  btag_mask & Xbb_mask & mass_mask 
    
    else:
        final_mask =  btag_mask & Xbb_mask
    
    return df[final_mask]

def main(args=None):
    args = parse_args(args)

    samples = glob.glob(str(args.samples) + "/**/*.root", recursive=True)

    if not os.path.exists(args.outDir):
        os.makedirs(args.outDir)

    outfile = args.outDir / args.outFile

    if outfile.exists() and args.overwrite:
        print(f"Overwriting {outfile}")
        os.remove(outfile)

    parquet_writer = None

    total_events=0

    for s in samples:
        print("Processing file", s)
        evt, cutflow = baseline_selections(s, year=args.year, mc=args.mc)

        if len(evt) == 0:
            continue

        print(f"Initial number of events: {cutflow['nEvents'][0]}")
        print(f"Number of events passing baseline selections: {len(evt)}")
        print(f"Selection Efficiency: {100*len(evt)/cutflow['nEvents'][0]:.2f}%", flush=True)
        total_events += len(evt)

        # Write the DataFrame to Parquet file
        table = pa.Table.from_pandas(evt, preserve_index=False)

        if parquet_writer is None:
            parquet_writer = pq.ParquetWriter(outfile, table.schema)

        parquet_writer.write_table(table)

    # Close the Parquet writer
    if parquet_writer:
        parquet_writer.close()

    print("Final number of events:", total_events)

    if args.post:
        print("Applying post-processing for flow training")
        df = pd.read_parquet(outfile)
        df = XbbPass(df)

        # Applying only 1 b-tag so we can look in SR
        btags = 1
        df = apply_cuts(df, btags=btags)

        # remove .parquet from outfile name so we can add _post
        out_post = outfile.with_suffix("")
        out_post = out_post.parent / (out_post.name + f"_flows_{btags}b.parquet")


        table_post = pa.Table.from_pandas(df, preserve_index=False)
        pq.write_table(table_post, out_post)

        print(f"Saving to {out_post}", flush=True)


if __name__ == "__main__":
    main()
