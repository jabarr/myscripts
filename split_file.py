#!/usr/bin/env python3

import argparse
import h5py
import os
import numpy as np
from tqdm import tqdm
import glob

def format_size(num):
    for unit in ['','K','M']:
        if abs(num) < 1000.0:
            return f"{int(num)}{unit}"
        num /= 1000.0
    return f"{num:.1f}G"

def split_h5_file(input_file, output_prefix, chunk_size, save_dir):
    with h5py.File(input_file, 'r') as f_in:
        datasets = {key: f_in[key] for key in f_in.keys()}
        total_entries = list(datasets.values())[0].shape[0]

        for start_index in range(0, total_entries, chunk_size):
            end_index = min(start_index + chunk_size, total_entries)
            output_file = os.path.join(save_dir, f"{output_prefix}_{format_size(start_index)}_{format_size(end_index)}.h5")

            with h5py.File(output_file, 'w') as f_out:
                for dataset_name, dataset in tqdm(datasets.items(), desc=f"Processing {output_file}", unit="dataset"):
                    dtype, shape = dataset.dtype, dataset.shape
                    new_shape = (end_index - start_index,) + shape[1:]
                    chunks = (100,) + shape[1:]

                    dset_out = f_out.create_dataset(
                        dataset_name,
                        shape=new_shape,
                        dtype=dtype,
                        chunks=chunks,
                        compression='lzf'
                    )
                    dset_out[:] = dataset[start_index:end_index]

                    # Copy attributes
                    for attr_name, attr_value in dataset.attrs.items():
                        dset_out.attrs[attr_name] = attr_value


def merge_h5_files(input_files, output_file):
    # Initialize empty dictionaries to store dataset information
    output_datasets = {}
    output_attributes = {}

    input_files = glob.glob(input_files)

    # Iterate through input files to determine the total size of each dataset
    for input_file in input_files:
        with h5py.File(input_file, 'r') as f_in:
            for dataset_name, dataset in f_in.items():
                if dataset_name not in output_datasets:
                    output_datasets[dataset_name] = dataset.shape[0]
                    output_attributes[dataset_name] = dict(dataset.attrs)
                else:
                    output_datasets[dataset_name] += dataset.shape[0]

    # Create the output file and datasets
    with h5py.File(output_file, 'w') as f_out:
        for dataset_name in output_datasets:
            with h5py.File(input_files[0], 'r') as f_in:
                dataset = f_in[dataset_name]
                dtype, shape = dataset.dtype, dataset.shape
                new_shape = (output_datasets[dataset_name],) + shape[1:]
                chunks = (100,) + shape[1:]

                dset_out = f_out.create_dataset(
                    dataset_name,
                    shape=new_shape,
                    dtype=dtype,
                    chunks=chunks,
                    compression='lzf'
                )

                # Copy attributes
                for attr_name, attr_value in output_attributes[dataset_name].items():
                    dset_out.attrs[attr_name] = attr_value

        # Iterate through input files again to copy data to the output file
        current_index = {dataset_name: 0 for dataset_name in output_datasets}
        for input_file in tqdm(input_files):
            with h5py.File(input_file, 'r') as f_in:
                for dataset_name, dataset in f_in.items():
                    dset_out = f_out[dataset_name]
                    start_index = current_index[dataset_name]
                    end_index = start_index + dataset.shape[0]
                    dset_out[start_index:end_index] = dataset[:]
                    current_index[dataset_name] = end_index

def select_rows(input_pattern, output_dir, chunk_size):
    input_files = glob.glob(input_pattern)
    
    with tqdm(total=len(input_files), desc='Processing files', unit='file') as pbar_files:
        for input_file in input_files:
            with h5py.File(input_file, 'r') as f_in:
                datasets = {key: f_in[key] for key in f_in.keys()}
                total_entries = len(datasets['jets'])

                output_file = os.path.join(output_dir, os.path.basename(input_file))

                with h5py.File(output_file, 'w') as f_out:
                    dsets_out = {}
                    for dataset_name, dataset in datasets.items():
                        dtype, shape = dataset.dtype, dataset.shape
                        dsets_out[dataset_name] = f_out.create_dataset(
                            dataset_name,
                            shape=(0,) + shape[1:],
                            maxshape=(None,) + shape[1:],
                            dtype=dtype,
                            chunks=(chunk_size,) + shape[1:],
                            compression='lzf'
                        )
                        for attr_name, attr_value in dataset.attrs.items():
                            dsets_out[dataset_name].attrs[attr_name] = attr_value

                    with tqdm(total=total_entries, desc=f'Processing {os.path.basename(input_file)}', unit='row', leave=False) as pbar_rows:
                        for start_index in range(0, total_entries, chunk_size):
                            end_index = min(start_index + chunk_size, total_entries)
                            select_indices = np.where(datasets['jets']['eventNumber'][start_index:end_index] % 10 == 9)[0]

                            if len(select_indices) == 0:
                                continue

                            select_indices += start_index

                            for dataset_name, dataset in datasets.items():
                                selected_data = dataset[select_indices]
                                dsets_out[dataset_name].resize((dsets_out[dataset_name].shape[0] + len(selected_data),) + dataset.shape[1:])
                                dsets_out[dataset_name][-len(selected_data):] = selected_data

                            pbar_rows.update(end_index - start_index)

            pbar_files.update(1)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Split, merge, or select rows in h5 files.')
    parser.add_argument('-m', '--mode',
                        choices=['split', 'merge', 'select'], 
                        required=True, 
                        help='Operation to be performed: split, merge or select')
    parser.add_argument('-i', '--input', 
                        required=True, 
                        help='Input for the operation. For split and select operations, this is the input file. For merge operation, these are the input files (multiple can be provided separated by space).')
    parser.add_argument('-o', '--output', 
                        required=True, 
                        help='Output for the operation. For split and select operations, this is the output prefix. For merge operation, this is the output file.')
    parser.add_argument('-c', '--chunk_size', 
                        type=int, 
                        default=10_000_000, 
                        help='Chunk size for the split and select operations')
    parser.add_argument('-s', '--save_dir', 
                        default='.', 
                        help='Directory where output files will be saved. Defaults to current directory.')

    args = parser.parse_args()

    if args.mode == 'split':
        split_h5_file(args.input, args.output, args.chunk_size, args.save_dir)
    elif args.mode == 'merge':
        merge_h5_files(args.input, os.path.join(args.save_dir, args.output))
    elif args.mode == 'select':
        select_rows(args.input, os.path.join(args.save_dir, args.output), args.chunk_size)

